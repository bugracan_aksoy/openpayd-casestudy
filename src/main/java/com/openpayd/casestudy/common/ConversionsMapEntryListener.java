package com.openpayd.casestudy.common;

import com.hazelcast.core.EntryEvent;
import com.hazelcast.map.listener.EntryAddedListener;
import com.hazelcast.map.listener.EntryRemovedListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @Author Bugracan Aksoy
 * @Date 22.01.2022
 * @Version 0.0.1
 * @Project casestudy
 */
public class ConversionsMapEntryListener implements
        EntryAddedListener<Long, String>,
        EntryRemovedListener<Long, String> {

    private final Logger log = LoggerFactory.getLogger(ConversionsMapEntryListener.class);


    @Override
    public void entryAdded(EntryEvent<Long, String> event) {
        log.debug("Conversion entry added : " + event);
    }

    @Override
    public void entryRemoved(EntryEvent<Long, String> event) {
        log.debug("Conversion entry removed : " + event);
    }

}