package com.openpayd.casestudy.common;

import com.openpayd.casestudy.data.exception.GetConversionsRequestException;
import com.openpayd.casestudy.data.exception.InvalidCurrencyException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.client.RestClientException;

import javax.servlet.http.HttpServletRequest;

/**
 * @Author Bugracan Aksoy
 * @Date 22.01.2022
 * @Version 0.0.1
 * @Project casestudy
 */

@ControllerAdvice
public class ExceptionHandler {

    private final Logger log = LoggerFactory.getLogger(ExceptionHandler.class);

    @org.springframework.web.bind.annotation.ExceptionHandler(value = Exception.class)
    public ResponseEntity globalExceptionHandler(Exception e, HttpServletRequest request) {
        log.error(String.valueOf(e.getStackTrace()));
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(e.getMessage());
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(value = RestClientException.class)
    public ResponseEntity restClientExceptionHandler(RestClientException e) {
        log.error(String.valueOf(e.getStackTrace()));
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(e.getMessage());
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(value = InvalidCurrencyException.class)
    public ResponseEntity restClientExceptionHandler(InvalidCurrencyException e) {
        log.error(String.valueOf(e.getStackTrace()));
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(e.getMessage());
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(value = GetConversionsRequestException.class)
    public ResponseEntity getConversionsExceptionHandler(GetConversionsRequestException e) {
        log.error(String.valueOf(e.getStackTrace()));
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(e.getMessage());
    }
}
