package com.openpayd.casestudy.common;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.map.IMap;
import com.openpayd.casestudy.data.Conversion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * @Author Bugracan Aksoy
 * @Date 22.01.2022
 * @Version 0.0.1
 * @Project casestudy
 */

@Component
public class HazelcastClient {

    public static final String CONVERSIONS = "conversions";

    @Qualifier("hazelcastInstance")
    @Autowired
    private HazelcastInstance hazelcastInstance;

    @PostConstruct
    private void postConstruct() {
        hazelcastInstance.getMap(CONVERSIONS).addEntryListener(new ConversionsMapEntryListener(), true);
    }

    public Conversion put(Long transactionId, Conversion conversion) {
        IMap<Long, Conversion> map = hazelcastInstance.getMap(CONVERSIONS);
        return map.putIfAbsent(transactionId, conversion);
    }

    public Conversion get(Long transactionId) {
        IMap<Long, Conversion> map = hazelcastInstance.getMap(CONVERSIONS);
        return map.get(transactionId);
    }

    public List<Conversion> getWithDate(Instant startDate, Instant endDate) {
        IMap<Long, Conversion> map = hazelcastInstance.getMap(CONVERSIONS);
        ArrayList<Conversion> conversions = new ArrayList<>();
        map.values().stream()
                .sorted(Comparator.comparing(Conversion::getId))
                .forEach(conversion -> {
                    if (conversion.getDate().isAfter(startDate) && conversion.getDate().isBefore(endDate)) {
                        conversions.add(conversion);
                    }
                });
        return conversions;
    }


}
