package com.openpayd.casestudy.controller;

import com.openpayd.casestudy.data.Conversion;
import com.openpayd.casestudy.data.request.ConversionApiRequest;
import com.openpayd.casestudy.data.request.GetConversionsApiRequest;
import com.openpayd.casestudy.service.ConversionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Author Bugracan Aksoy
 * @Date 22.01.2022
 * @Version 0.0.1
 * @Project casestudy
 */

@RestController
@RequestMapping("/api")
public class ConversionController {

    private final Logger log = LoggerFactory.getLogger(ConversionController.class);

    @Autowired
    @Qualifier("conversionServiceImpl")
    ConversionService conversionService;

    @PostMapping("/saveConversion")
    public ResponseEntity<Conversion> saveConversion(@RequestBody ConversionApiRequest conversionApiRequest) throws Exception {
        log.debug("REST - saveConversion : {}", conversionApiRequest);
        Conversion result = conversionService.save(conversionApiRequest);
        return ResponseEntity.ok().body(result);

    }

    @PostMapping("/getConversions")
    public ResponseEntity<List<Conversion>> getConversions(@RequestBody GetConversionsApiRequest request) throws Exception {
        log.debug("REST - getConversions : {}", request);
        List<Conversion> result = conversionService.getConversions(request);
        return ResponseEntity.ok().body(result);
    }
}
