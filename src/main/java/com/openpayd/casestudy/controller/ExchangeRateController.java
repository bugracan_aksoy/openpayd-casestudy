package com.openpayd.casestudy.controller;

import com.openpayd.casestudy.service.ExchangeRateService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

/**
 * @Author Bugracan Aksoy
 * @Date 22.01.2022
 * @Version 0.0.1
 * @Project casestudy
 */

@RestController
@RequestMapping("/api")
public class ExchangeRateController {

    private final Logger log = LoggerFactory.getLogger(ExchangeRateController.class);


    @Autowired
    @Qualifier("exchangeRateServiceImpl")
    ExchangeRateService exchangeRateService;

    @GetMapping("/getExchangeRate")
    public ResponseEntity<BigDecimal> getExchangeRate(@RequestParam String source, @RequestParam String target) throws Exception {
        log.debug("REST - getExchangeRate : {} to : {}", source, target);
        try {
            final BigDecimal rate = exchangeRateService.convert(source, target);
            return ResponseEntity.ok().body(rate);
        } catch (Exception e) {
            throw new Exception();
        }
    }

}
