package com.openpayd.casestudy.data;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;

/**
 * @Author Bugracan Aksoy
 * @Date 22.01.2022
 * @Version 0.0.1
 * @Project casestudy
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Conversion implements Serializable {
    private Long id;
    private String source;
    private String target;
    private BigDecimal amount;
    private BigDecimal exchangeRate;
    private BigDecimal total;
    public Instant date;

}
