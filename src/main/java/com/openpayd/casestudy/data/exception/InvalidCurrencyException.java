package com.openpayd.casestudy.data.exception;

/**
 * @Author Bugracan Aksoy
 * @Date 22.01.2022
 * @Version 0.0.1
 * @Project casestudy
 */
public class InvalidCurrencyException extends Exception {
}
