package com.openpayd.casestudy.data.request;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @Author Bugracan Aksoy
 * @Date 22.01.2022
 * @Version 0.0.1
 * @Project casestudy
 */
@Data
public class ConversionApiRequest {
    private String source;
    private String target;
    private BigDecimal amount;
}
