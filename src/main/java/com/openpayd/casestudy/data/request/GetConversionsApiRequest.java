package com.openpayd.casestudy.data.request;

import lombok.Data;

import java.io.Serializable;
import java.time.Instant;

/**
 * @Author Bugracan Aksoy
 * @Date 22.01.2022
 * @Version 0.0.1
 * @Project casestudy
 */
@Data
public class GetConversionsApiRequest extends PageableApiRequest implements Serializable {
    private Instant startDate;
    private Instant endDate;
    private Long transactionId;
}
