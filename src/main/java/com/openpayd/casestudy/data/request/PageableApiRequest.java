package com.openpayd.casestudy.data.request;

import lombok.Data;

/**
 * @Author Bugracan Aksoy
 * @Date 22.01.2022
 * @Version 0.0.1
 * @Project casestudy
 */
@Data
public class PageableApiRequest {
    private int pageIndex;
    private int pageSize;
    private String sortOrder;
    private String sortColumn;
}
