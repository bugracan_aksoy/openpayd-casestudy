package com.openpayd.casestudy.data.response;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

/**
 * @Author Bugracan Aksoy
 * @Date 22.01.2022
 * @Version 0.0.1
 * @Project casestudy
 */
@Data
public class ExchangeRateResponse {
    private boolean success;
    private Date date;
    private String base;
    private Map<String, BigDecimal> rates;
}
