package com.openpayd.casestudy.service;

import com.openpayd.casestudy.data.request.ConversionApiRequest;
import com.openpayd.casestudy.data.request.GetConversionsApiRequest;
import com.openpayd.casestudy.data.Conversion;

import java.util.List;

/**
 * @Author Bugracan Aksoy
 * @Date 22.01.2022
 * @Version 0.0.1
 * @Project casestudy
 */

public interface ConversionService {
    Conversion save(ConversionApiRequest conversionApiRequest) throws Exception;
    List<Conversion> getConversions(GetConversionsApiRequest getConversionsApiRequest) throws Exception;
}
