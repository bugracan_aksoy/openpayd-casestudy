package com.openpayd.casestudy.service;

import java.math.BigDecimal;

/**
 * @Author Bugracan Aksoy
 * @Date 22.01.2022
 * @Version 0.0.1
 * @Project casestudy
 */

public interface ExchangeRateService {
    BigDecimal convert(String source, String target) throws Exception;
}
