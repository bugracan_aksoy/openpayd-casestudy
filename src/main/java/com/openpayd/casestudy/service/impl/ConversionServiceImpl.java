package com.openpayd.casestudy.service.impl;

import com.openpayd.casestudy.common.HazelcastClient;
import com.openpayd.casestudy.data.exception.GetConversionsRequestException;
import com.openpayd.casestudy.data.exception.InvalidCurrencyException;
import com.openpayd.casestudy.data.request.ConversionApiRequest;
import com.openpayd.casestudy.data.request.GetConversionsApiRequest;
import com.openpayd.casestudy.data.Conversion;
import com.openpayd.casestudy.data.response.ExchangeRateResponse;
import com.openpayd.casestudy.service.ConversionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Currency;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @Author Bugracan Aksoy
 * @Date 22.01.2022
 * @Version 0.0.1
 * @Project casestudy
 */

@Service("conversionServiceImpl")
public class ConversionServiceImpl implements ConversionService {

    @Autowired
    private final RestTemplate restTemplate;

    @Autowired
    private final HazelcastClient hazelcastClient;


    private final String api;
    private static AtomicInteger ID_GENERATOR = new AtomicInteger(10000);

    public ConversionServiceImpl(RestTemplate restTemplate, HazelcastClient hazelcastClient, @Value("${exchangeratesio.url}") String api) {
        this.restTemplate = restTemplate;
        this.hazelcastClient = hazelcastClient;
        this.api = api;
    }

    @Override
    public Conversion save(ConversionApiRequest conversionApiRequest) throws Exception {
        Currency sourceCurrency = Currency.getInstance(conversionApiRequest.getSource());
        Currency targetCurrency = Currency.getInstance(conversionApiRequest.getTarget());
        if (sourceCurrency != null && targetCurrency != null) {
            ExchangeRateResponse response = restTemplate.getForObject(api, ExchangeRateResponse.class);
            BigDecimal exchangeRate = response.getRates().get(targetCurrency.getCurrencyCode())
                    .divide(response.getRates().get(sourceCurrency.getCurrencyCode()), RoundingMode.HALF_UP);
            Conversion conversion = Conversion.builder()
                    .amount(conversionApiRequest.getAmount())
                    .date(Instant.now())
                    .exchangeRate(exchangeRate)
                    .source(conversionApiRequest.getSource())
                    .target(conversionApiRequest.getTarget())
                    .total(conversionApiRequest.getAmount().multiply(exchangeRate))
                    .id(Long.valueOf(ID_GENERATOR.getAndIncrement()))
                    .build();
            hazelcastClient.put(conversion.getId(), conversion);
            return conversion;
        } else {
            throw new InvalidCurrencyException();
        }
    }

    @Override
    public List<Conversion> getConversions(GetConversionsApiRequest request) throws Exception {

        if (request.getTransactionId() != null) {
            return new ArrayList<>(Arrays.asList(hazelcastClient.get(request.getTransactionId())));
        } else if (request.getStartDate() != null || request.getEndDate() != null) {
            List<Conversion> resultSet = hazelcastClient.getWithDate(request.getStartDate(), request.getEndDate());

            if (request.getPageSize() != 0 || request.getPageIndex() != 0) {
                int startIndex = 0;
                if ((request.getPageIndex() * request.getPageSize()) - request.getPageSize() > 0) {
                    startIndex = (request.getPageIndex() * request.getPageSize()) - request.getPageSize();
                }

                int index = 0;
                List<Conversion> conversionsWithPaging = new ArrayList<>();
                for (Conversion conversion : resultSet) {
                    if ((startIndex <= index && index < startIndex + request.getPageSize())) {
                        conversionsWithPaging.add(conversion);
                    }
                    index++;
                }
                return conversionsWithPaging;
            }
            return resultSet;
        } else {
            throw new GetConversionsRequestException();
        }

    }
}
