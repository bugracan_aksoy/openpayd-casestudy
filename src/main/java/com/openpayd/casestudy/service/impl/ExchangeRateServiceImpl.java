package com.openpayd.casestudy.service.impl;

import com.openpayd.casestudy.data.response.ExchangeRateResponse;
import com.openpayd.casestudy.service.ExchangeRateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Currency;

/**
 * @Author Bugracan Aksoy
 * @Date 22.01.2022
 * @Version 0.0.1
 * @Project casestudy
 */

@Service("exchangeRateServiceImpl")
public class ExchangeRateServiceImpl implements ExchangeRateService {

    @Autowired
    private final RestTemplate restTemplate;
    private final String api;

    public ExchangeRateServiceImpl(RestTemplate restTemplate, @Value("${exchangeratesio.url}") String api) {
        this.restTemplate = restTemplate;
        this.api = api;
    }

    @Override
    public BigDecimal convert(String source, String target) throws Exception {
        try {
            Currency sourceCurrency = Currency.getInstance(source);
            Currency targetCurrency = Currency.getInstance(target);
            if(sourceCurrency !=null && targetCurrency!=null){
                ExchangeRateResponse response = restTemplate.getForObject(api, ExchangeRateResponse.class);
                return response.getRates().get(targetCurrency.getCurrencyCode())
                        .divide(response.getRates().get(sourceCurrency.getCurrencyCode()), RoundingMode.HALF_UP);

            }else {
                throw new Exception();
            }
        } catch (RestClientException e) {
            throw new Exception(e.getMessage());
        }
    }

}
