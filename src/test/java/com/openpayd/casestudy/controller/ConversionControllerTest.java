package com.openpayd.casestudy.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.openpayd.casestudy.data.request.ConversionApiRequest;
import com.openpayd.casestudy.data.request.GetConversionsApiRequest;
import com.openpayd.casestudy.service.ConversionService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

/**
 * @Author Bugracan Aksoy
 * @Date 22.01.2022
 * @Version 0.0.1
 * @Project casestudy
 */

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = ConversionController.class)
public class ConversionControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    @Qualifier("conversionServiceImpl")
    private ConversionService conversionService;

    @Test
    void testSaveConversionMethod() throws Exception {
        final ConversionApiRequest request = new ConversionApiRequest();
        ObjectMapper objectMapper = new ObjectMapper();
        request.setAmount(BigDecimal.valueOf(10));
        request.setSource("USD");
        request.setTarget("GBP");
        mockMvc.perform(
                post("/api/saveConversion")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content((objectMapper.writeValueAsString(request))));
        Mockito.verify(conversionService).save(request);
    }

    @Test
    void testGetConversionsMethod() throws Exception {
        final GetConversionsApiRequest request = new GetConversionsApiRequest();
        ObjectMapper objectMapper = new ObjectMapper();
        request.setTransactionId(Long.valueOf(10000));
        mockMvc.perform(
                post("/api/getConversions")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content((objectMapper.writeValueAsString(request))));
        Mockito.verify(conversionService).getConversions(request);
    }

}
