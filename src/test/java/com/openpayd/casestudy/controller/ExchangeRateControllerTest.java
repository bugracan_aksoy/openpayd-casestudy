package com.openpayd.casestudy.controller;

import com.openpayd.casestudy.service.ExchangeRateService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @Author Bugracan Aksoy
 * @Date 22.01.2022
 * @Version 0.0.1
 * @Project casestudy
 */

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = ExchangeRateController.class)
public class ExchangeRateControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    @Qualifier("exchangeRateServiceImpl")
    private ExchangeRateService exchangeRateService;

    @Test
    void testConvertMethod() throws Exception {
        Mockito.doReturn(BigDecimal.TEN)
                .when(exchangeRateService)
                .convert("USD", "GBP");

        mockMvc.perform(get("/api/getExchangeRate?source=USD&target=GBP"))
                .andExpect(status().isOk());
    }
}
